package main.java;

import main.java.animals.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kot = new Kotik("Борис", "мяу", 1, 4);
        Kotik heka = new Kotik();
        heka.liveAnotherDay();
        heka.getName();
        heka.getWeight();
        System.out.println("Колличество котиков:" + Kotik.getCount());
        compareVoice(heka, kot);
    }
    public static void compareVoice(Kotik a, Kotik b) {
        System.out.println(a.getVoice() == b.getVoice());
    }
}
