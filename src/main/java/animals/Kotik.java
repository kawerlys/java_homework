package main.java.animals;

public class Kotik {
    private String name;
    private String voice;
    private int satiety;
    private int weight;
    private final int METHODS = 5;
    private static int count = 0;

    public Kotik() {
        Kotik.count++;
    }

    public Kotik(String name, String voice, int satiety, int weight) {
        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        Kotik.count++;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
    public String getVoice() {
        return voice;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }
    public int getSatiety() {
        return satiety;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    public int getWeight() {
        return weight;
    }

    public static int getCount(){
        return count;
    }

    boolean play(){
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    boolean sleep(){
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    boolean wash() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    boolean walk() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    boolean hunt(){
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    void eat(int a) {
        satiety+=a;
    }

    void eat(int satiety, String food) {

    }

    void eat() {
        eat(5, "Баклажан");
    }

    public void liveAnotherDay() {
        int f;
        String g;
        String arr[] = new String[25];
        for (int j = 0; j < 25; j++) {
            f = (int) (Math.random() * METHODS) + 1;

            switch(f) {
                case 1:
                    if (sleep()) {
                        g = Integer.toString(j);
                        arr[j] = g + " - спал";
                    } else {
                        g = Integer.toString(j);
                        arr[j] = g + " - кушал";
                        eat(1);
                    }
                    break;
                case 2:
                    if(wash()) {
                        g = Integer.toString(j);
                        arr[j] = g + " - мылся";
                    } else {
                        g = Integer.toString(j);
                        arr[j] = g + " - кушал";
                        eat(1);
                    }
                    break;
                case 3:
                    if(walk()) {
                        g = Integer.toString(j);
                        arr[j] = g + " - гулял";
                    } else {
                        g = Integer.toString(j);
                        arr[j] = g + " - кушал";
                        eat(1);
                    }
                    break;
                case 4:
                    if(play()){
                        g = Integer.toString(j);
                        arr[j] = g + " - играл";
                    } else {
                        g = Integer.toString(j);
                        arr[j] = g + " - кушал";
                        eat(1);
                    }
                    break;
                case 5:
                    if(hunt()) {
                        g = Integer.toString(j);
                        arr[j] = g + " - охотился";
                    } else {
                        g = Integer.toString(j);
                        arr[j] = g + " - кушал";
                        eat(1);
                    }
                    break;
            }
        }
        for (int i = 0; i < 25; i++) {
            System.out.println(arr[i]);
        }
    }
}

